<?php

include_once 'config.php';

$cnx = '';

function conectar() {
    global $cnx;
    $cnx = @mysqli_connect(HOST, USER, PASS, DATABASE, PORT) or die('Error en la conexion');
    mysqli_query($cnx, 'set names utf8');
}

function consultar($sql) {
    global $cnx;
    conectar();
    $bolsa = mysqli_query($cnx,$sql);
    $datos = array();
    while ($f = mysqli_fetch_assoc($bolsa)) {
        $datos[] = $f;
    }
    desconectar();
    return $datos;
}

function ejecutar($sql) {
    global $cnx;
    conectar();
    $exito = mysqli_query($cnx, $sql);
    if($exito==1 or $exito==true){
        return 1;
    } else {
        return 0;
    }
    desconectar();
}

function desconectar() {
    global $cnx;
    mysqli_close($cnx);
}

?>
