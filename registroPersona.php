<?php
include_once 'lib_mysql.php';
if ($_GET) {
    # UPDATE
    $id = $_GET['elegido'];
    $sql = "select * from personas where id='$id'";
    $tempo = consultar($sql);
    $paterno = $tempo[0]['paterno'];
    $materno = $tempo[0]['materno'];
    $nombre = $tempo[0]['nombre'];
    $cumple = $tempo[0]['cumple'];
    $tipoDocum = $tempo[0]['docu_id'];
    $nroDocum = $tempo[0]['docu_numero'];
    $correo = $tempo[0]['correo'];
} else {
    # INSERT
    $id = '';
    $paterno = '';
    $materno = '';
    $nombre = '';
    $cumple = '';
    $tipoDocum = '';
    $nroDocum = '';
    $correo = '';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Registro</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="ml-2">
            <label>Registro de nueva persona</label>
            <br>
            <form method="POST" action="grabar.php">
                <div class="form-group">
                    <label>ID</label> 
                    <input type="text" name="txtid" value="<?= $id ?>" class="form-control w-25" readonly>
                </div>
                <div class="form-group">
                    <label>Apellido paterno</label>
                    <input type="text" name="txtpaterno" maxlength="50" value="<?= $paterno ?>" class="form-control w-75" required>
                </div>
                <div class="form-group">
                    <label>Apellido materno: </label>
                    <input type="text" name="txtmaterno" maxlength="50" value="<?= $materno ?>" class="form-control w-75" required>
                </div>
                <div class="form-group">
                    <label>Nombres:</label> 
                    <input type="text" name="txtnombre" maxlength="50" value="<?= $nombre ?>"  class="form-control w-75" required>
                </div>
                <div class="form-group">
                    <label>Fecha de cumpleaños:</label> 
                    <input type="date" name="txtcumple" maxlength="50" value="<?= $cumple ?>" class="form-control w-25" required>
                </div>
                <div class="form-group">
                    <label>Tipo de documento:</label>
                    <select name="cboDocumento" class="form-control w-75" required>
                        <option></option>
                        <option value="1" <?= ($tipoDocum == '1' ? 'selected' : '') ?>>DNI</option>
                        <option value="2" <?= ($tipoDocum == '2' ? 'selected' : '') ?>>PARTIDA</option>
                        <option value="3" <?= ($tipoDocum == '3' ? 'selected' : '') ?>>PASAPORTE</option>
                        <option value="4" <?= ($tipoDocum == '4' ? 'selected' : '') ?>>CARNE DE EXTRANJERIA</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Numero de documento:</label> 
                    <input type="text" name="txtnumerodoc" maxlength="50" value="<?= $nroDocum ?>" class="form-control w-75" required>
                </div>
                <div class="form-group">
                    <label>Correo:</label> 
                    <input type="email" name="txtcorreo" maxlength="50" value="<?= $correo ?>" class="form-control w-75" required>
                </div>
                <div class="form-group">
                    <input type="submit" value="Registrar" class="btn btn-primary">
                </div>
            </form>
        </div>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
