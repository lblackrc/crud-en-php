<?php
# Inicializando para hacer uso de SESIONES
session_start();
?>

<?php
include_once 'lib_mysql.php';

if (isset($_POST['botonEnvio'])) {

    $userName = $_POST['txtUser'];
    $password = $_POST['txtPassword'];

    if ($userName != "" && $password != "") {
        # query para comprobar si el usuario ingresado se encuentra o no en la tabla de usuarios de la BD
        $sql = "select count(*) as contadorUsuario from usuarios where usuario='$userName' and clave='$password'";
        $resultado = consultar($sql); # array con los datos extraidos
        $existe = $resultado[0]['contadorUsuario']; # obteniendo el numero de veces que aparece el usuario ingresado

        if ($existe > 0) {
            $_SESSION['username'] = $userName;
            #echo $_SESSION['username'];
            echo "<script>window.location.replace('index.php')</script>";
            #header('Location: index.php');
        } else {
            echo "<br>";
            echo "<script>alert('Usuario y contraseña inválidos. Por favor, ingrese nuevamente.')</script>";
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="d-flex align-items-center min-vh-100">
            <div class="container text-center">
                <form method="POST" action="">
                    <div id="login">
                        <h3>Iniciar sesión</h3>
                        <div>
                            <input type="text" name="txtUser" placeholder="Usuario" maxlength="30" required>
                        </div>
                        <br>
                        <div>
                            <input type="password" name="txtPassword" placeholder="Contraseña" maxlength="30" required>
                        </div>
                        <br>
                        <div>
                            <input type="submit" name="botonEnvio" value="Ingresar" class="btn btn-primary">
                        </div>
                        <br>
                        <div>
                            <a href="registro.php">¿Eres nuevo? ¡Registrate!<a/> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="js/bootstrap.min.js"></script>
    </body>

</html>

