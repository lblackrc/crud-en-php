<?php
session_start();

# verificando si el usuario se encuentra logeado o no
if (!isset($_SESSION['username'])) {
    echo "<script>window.location.replace('login.php')</script>";
    #header('Location: login.php');  # si el usuario no se logeo previamente, se le redirige a la pagina de login 
}

$user = $_SESSION['username'];
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Configuración</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="ml-2">
            <div class="form-group">Configuración de la cuenta de <?= $user ?></div>
            <form method="POST" action="">
                <div class="form-group">
                    <label>Nuevo nombre de usuario</label>
                    <input type="text" name="user" value="" class="form-control w-75" maxlength="30" required>
                </div>
                <div class="form-group">
                    Contraseña anterior
                    <input type="password" name="oldpass" class="form-control w-75" maxlength="30" required>
                </div>
                <div class="form-group">
                    Nueva contraseña
                    <input type="password" name="newpass" class="form-control w-75" maxlength="30" required>
                </div>
                <div class="form-group">
                    <input type="submit" value="Confirmar" class="btn btn-primary d-inline-block">
                </div>
            </form>
            <form method="POST" action="">
                <div class="form-group">
                    <input type="submit" name ="deleteacc" value="Darse de baja" class="btn btn-danger"
                           onclick="return confirm('¿Está seguro que desea eliminar su cuenta de usuario?')">
                </div>
            </form>
        </div>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>

<?php
if ($_POST) {
    include_once 'lib_mysql.php';

    if (isset($_POST['deleteacc'])) {
        $sql = "delete from usuarios where usuario='$user'";
        $exito = ejecutar($sql);
        if ($exito == 1) {
            session_destroy();
            echo "<script>window.location.replace('login.php')</script>";
            #header('Location: login.php');
        }
    } else {
        $sql = "select * from usuarios where usuario='$user'";
        $resultado = consultar($sql);

        $oldpass = $resultado[0]['clave'];
        $newpass = $_POST['newpass'];
        $newuser = $_POST['user'];

        if ($oldpass != $_POST['oldpass']) {
            echo "Las contraseñas no coinciden. Por favor, ingreselas nuevamente.";
        } else {
            $sql = "update usuarios set usuario='$newuser', clave='$newpass' where usuario='$user'";
            $exito = ejecutar($sql);
            if ($exito == 1) {
                $_SESSION['modificacionUsuario'] = 'Exito';
                $_SESSION['username'] = "$newuser";
                echo "<script>window.location.replace('index.php')</script>";
                #header('Location: index.php');
            }
        }
    }
}
?>
