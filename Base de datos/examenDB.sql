-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.6058
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para id14754986_php1_2020_examen
CREATE DATABASE IF NOT EXISTS `id14754986_php1_2020_examen` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `id14754986_php1_2020_examen`;

-- Volcando estructura para tabla php1_2020_examen.documentos
CREATE TABLE IF NOT EXISTS `documentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla php1_2020_examen.documentos: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
INSERT INTO `documentos` (`id`, `descripcion`) VALUES
	(4, 'CARNE EXTRANJERIA'),
	(1, 'DNI'),
	(2, 'PARTIDA'),
	(3, 'PASAPORTE');
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;

-- Volcando estructura para tabla php1_2020_examen.personas
CREATE TABLE IF NOT EXISTS `personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paterno` varchar(50) NOT NULL,
  `materno` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cumple` date NOT NULL,
  `docu_id` int(11) NOT NULL,
  `docu_numero` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `correo_flag` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__documentos` (`docu_id`),
  CONSTRAINT `FK__documentos` FOREIGN KEY (`docu_id`) REFERENCES `documentos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla php1_2020_examen.personas: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` (`id`, `paterno`, `materno`, `nombre`, `cumple`, `docu_id`, `docu_numero`, `correo`, `correo_flag`) VALUES
	(1, 'jimenez', 'torres', 'martin', '2020-06-10', 1, '12345678', 'percy@3soft.pe', 1),
	(3, 'a3', 'b3', 'c3', '2020-06-24', 1, '1111', 'percy.almeyda@gmail.com', 1),
	(4, 'a4', 'a4', 'a4', '2020-06-25', 1, '2222', 'percy.almeyda@gmail.com', 1),
	(5, 'a5', 'b5', 'c5', '2020-06-26', 1, '22222', 'percy.almeyda@gmail.com', 1),
	(6, 'a6', 'a6', 'a6', '2020-06-26', 1, '333', 'percy.almeyda@gmail.com', 1),
	(8, 'huertas', 'rehuertas', 'florencia', '2020-07-06', 1, '12345678', 'percy.almeyda@gmail.com', 1),
	(9, 'Almeyda', 'Levano', 'Percy', '2020-07-02', 1, '22093104', 'percy@3soft.pe', 1);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;

-- Volcando estructura para tabla php1_2020_examen.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) DEFAULT NULL,
  `clave` varchar(30) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla php1_2020_examen.usuarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `usuario`, `clave`, `estado`) VALUES
	(1, 'admin', 'admin', 1),
	(2, 'user', 'user', 1),
	(4, 'visita', 'visita', 1),
	(5, 'a1', 'a1', 1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Volcando estructura para vista php1_2020_examen.vpersonas
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `vpersonas` (
	`id` INT(11) NOT NULL,
	`persona` VARCHAR(152) NULL COLLATE 'latin1_swedish_ci',
	`cumple` DATE NOT NULL,
	`docu_id` INT(11) NOT NULL,
	`docu_tipo` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`docu_numero` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`correo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`correo_flag` TINYINT(4) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista php1_2020_examen.vpersonas
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `vpersonas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vpersonas` AS SELECT a.id,CONCAT_WS(' ',a.paterno,a.materno,a.nombre) persona,
a.cumple,a.docu_id,b.descripcion as docu_tipo,a.docu_numero,a.correo,a.correo_flag 
FROM personas a 
left JOIN documentos b ON a.docu_id=b.id ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
