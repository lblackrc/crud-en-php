<?php

session_start();

# Exportando la data a Excel
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=personas.xls"); 

include_once "config.php";

// Check user login or not
if(!isset($_SESSION['username'])){
    echo "<script>window.location.replace('login.php')</script>";
    #header('Location: login.php');
}

// logout
if(isset($_POST['botonLogout'])){
    session_destroy();
    echo "<script>window.location.replace('login.php')</script>";
    #header('Location: login.php');
}



include_once 'lib_mysql.php';
$sql = 'select * from personas';
$tempo = consultar($sql);

?>

<!doctype html>
<html>
    <head>
		<meta charset="UTF-8">
	</head>
    <body>
        <table border="1">
            <tr>
                <td>ID</td>
                <td>Apellido paterno</td>
                <td>Apellido materno</td>
                <td>Nombres</td>
                <td>Cumpleaños</td>
                <td>Tipo documento</td>
                <td>Nro Documento</td>
                <td>Correo</td>
                <td>Correo_flag</td>
            </tr>
            <?php foreach ($tempo as $w){?>
            <tr>
                <td><?= $w['id']?></td>
                <td><?= $w['paterno']?></td>
                <td><?= $w['materno']?></td>
                <td><?= $w['nombre']?></td>
                <td><?= $w['cumple']?></td>
                <td><?= $w['docu_id']?></td>
                <td><?= $w['docu_numero']?></td>
                <td><?= $w['correo']?></td>
                <td><?= $w['correo_flag']?></td>
            </tr>
            <?php } ?>
        </table>
        
        <br>     
    </body>
</html>

