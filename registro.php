<!DOCTYPE html>
<html>
    <head>
        <title>Registro de usuario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="form-group ml-2">Formulario de registro</div>
        <form method="POST" action="" class="ml-2">
            <div class="form-group">
                <label>Nombre de usuario</label> 
                <input type="text" name="txtUser" class="form-control w-25" maxlength="30" required>
            </div>
            <div class="form-group">
                <label>Contraseña</label>
                <input type="text" name="txtPassword" class="form-control w-25" maxlength="30" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Registrar" class="btn btn-primary">
            </div>    
        </form>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>

<?php
if ($_POST) {
    $user = $_POST['txtUser'];
    $pass = $_POST['txtPassword'];

    include_once 'lib_mysql.php';
    if ($user != "") {
        $sql = "select count(*) as contadorUsuario from usuarios where usuario='$user'";
        $resultado = consultar($sql); # array con los datos extraidos
        $existe = $resultado[0]['contadorUsuario'];

        if ($existe > 0) {
            echo "<script>alert('El nombre de usuario $user ya se encuentra en uso' + 
            'por favor registre un nombre de usuario diferente')</script>";
        } else {
            $sql = "insert into usuarios(usuario,clave,estado) ";
            $sql .= "values('$user','$pass','1')";

            $estado = ejecutar($sql);
            if ($estado == 1) {
                echo "<script>window.location.replace('login.php')</script>";
                #header('location: login.php');
            } else {
                echo "Error al insertar.<br> $sql";
            }
        }
    }
}
?>
