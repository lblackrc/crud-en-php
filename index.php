<?php
# inicializando para hacer uso de la sesion previamente guardada
session_start();
include_once "config.php";

# para mostrar el mensaje de alerta de configuracion de cuenta exitosa
if (isset($_SESSION['modificacionUsuario'])) {
    echo "<script>alert('Datos de usuario modificados satisfactoriamente')</script>";
    unset($_SESSION['modificacionUsuario']); # para que no aparezca la alerta nuevamente despues de refrescar la pagina
}

# para mostrar el mensaje de alerta de notificacion exitosa
if (isset($_SESSION['notificacion'])) {
    echo "<script>alert('La notificación se ha realizado con éxito')</script>";
    unset($_SESSION['notificacion']); # para que no aparezca la alerta nuevamente despues de refrescar la pagina
}

# verificando si el usuario se encuentra logeado o no
if (!isset($_SESSION['username'])) {
    echo "<script>window.location.replace('login.php')</script>";
    #header('Location: login.php');  # si el usuario no se logeo previamente, se le redirige a la pagina de login 
}

# finalizando sesion en caso de que el usuario lo haya solicitado
if (isset($_POST['botonLogout'])) {
    session_destroy();
    echo "<script>window.location.replace('login.php')</script>";
    #header('Location: login.php');
}

# consultando a la BD el contenido de la tabla 'personas'
include_once 'lib_mysql.php';
$sql = 'select * from personas';
$tempo = consultar($sql);
?>

<!doctype html>
<html>
    <head>
        <title>Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>

    <body>
        <h2>Bienvenido <?= $_SESSION['username'] ?></h2>
        <div class="bg-dark">
            <div class="d-inline-block w-25 text-center">
                <a href="registroPersona.php" class="text-white">Registrar Persona</a>
            </div>
            <div class="d-inline-block w-25 text-center">
                <a href="excel.php" class="text-white">Exportar a Excel</a>
            </div>
            <div class="d-inline-block w-25 text-center">
                <a href="configurarUsuario.php" class="text-white">Configuracion de cuenta</a>
            </div>
            <div class="d-inline-block w-24">
                <form method='POST' action=""  class="d-inline">
                    <input type="submit" value="Cerrar sesión" name="botonLogout" class="btn btn-primary">
                </form>
            </div>
        </div>
        <br>
        <table border="1" class="table table-striped">
            <tr class="bg-info text-center text-white">
                <td>ID</td>
                <td>Apellido paterno</td>
                <td>Apellido materno</td>
                <td>Nombres</td>
                <td>Cumpleaños</td>
                <td>Tipo documento</td>
                <td>Nro Documento</td>
                <td>Correo</td>
                <td>Correo_flag</td>
                <td>Acciones</td>
                <td>Notificar</td>
            </tr>
            <?php foreach ($tempo as $w) { ?>
                <tr>
                    <td><?= $w['id'] ?></td>
                    <td><?= $w['paterno'] ?></td>
                    <td><?= $w['materno'] ?></td>
                    <td><?= $w['nombre'] ?></td>
                    <td><?= $w['cumple'] ?></td>
                    <td><?= $w['docu_id'] ?></td>
                    <td><?= $w['docu_numero'] ?></td>
                    <td><?= $w['correo'] ?></td>
                    <td><?= $w['correo_flag'] ?></td>
                    <td><a href="registroPersona.php?elegido=<?= $w['id'] ?>">Editar</a> | 
                        <a href="borrar.php?elegido=<?= $w['id'] ?>" 
                           onclick="return confirm('¿Está seguro de borrar a la persona seleccionada?')">Borrar</a></td>
                    <td><a href="mail.php?elegido=<?= $w['id'] ?>">Enviar correo</a></td>
                </tr>
            <?php } ?>
        </table>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>

